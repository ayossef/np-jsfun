const passRules = require('./validator-rules')
let pass = {
    value: '',
    hasValidLength: () => {
        return hasValidLength(this.value) 
            && hasLegalChars(this.value)
    },
    hasLegalChars: () => {
        return passRules.deosContainReqChar(this.value)
        && passRules.doesNotContainIlligealChar(this.value)
    },
    isValid: ()=>{
        return this.hasValidLength() 
        && this.hasLegalChars()
    }
}


pass.value = '123344444'
console.log(pass.isValid());



// function validPassword(passToBeValidated){
//     return hasValidLength(passToBeValidated) 
//         && hasLegalChars(passToBeValidated)
// }

// function hasValidLength(passToBeValidated){
//     return passRules.isMoreMinThanLength(passToBeValidated) 
//         && passRules.isLessThanMaxLength(passToBeValidated)
// }

// function hasLegalChars(passToBeValidated){
//     return passRules.deosContainReqChar(passToBeValidated)
//         && passRules.doesNotContainIlligealChar(passToBeValidated)
// }

// module.exports={validPassword}