// function validatePassword
// at least 8 char - at most 20 char
// should contain a number and one of these chars [@,$,!]

passwordString = "qwertyui!"
// passwordString = "qwertyui"
// passwordString = "qwert"

emailString = "test@gmail.com"
// emailString = "test@yahoo.com"
// emailString = "test"

// console.log(validatePassword(passwordString))
console.log(validateEmail(emailString))

function validatePassword(passwordString){
   
    return (passwordString.length < 8 && passwordString.length > 20)&&
    (passwordString.includes("@")||passwordString.includes("!")||passwordString.includes("$"))
}


// function validateEmail
// should have . 
// should have @
// allowed domains [gmail.com, yahoo.com]


function validateEmail(emailString){

    return (emailString.includes("@") && emailString.includes("."))&&
    (emailString.includes("gmail.com") || emailString.includes("yahoo.com"))

}