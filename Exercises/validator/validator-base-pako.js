console.log(validatePassword('ff'))
console.log(validateEmail('me@gmail.com'));
// function validatePassword
// at least 8 char - at most 20 char
// should contain a number and one of these chars [@,$,!]
function validatePassword(
    password, 
    minLength = 8, 
    maxLength = 20, 
    hasNumber = true, 
    specialChars = ['@','$','!']
) {
    return password 
    && password.length >= minLength
    && password.length <= maxLength
    && (hasNumber) ? /\d/.test(password) : true
    && specialChars.some(c => password.split("").includes(c))

}

// function validateEmail
// should have . 
// should have @
// allowed domains [gmail.com, yahoo.com]
function validateEmail(email, allowedDomains = ['gmail.com', 'yahoo.com'], specialChars = ['.', '@']) {
    return email
    && allowedDomains.some(d => email.includes(d))
    && email.split("").every(c => specialChars.includes(c))

}