// function validatePassword
// at least 8 char - at most 20 char
// should contain a number and one of these chars [@,$,!]

// function validateEmail
// should have . 
// should have @
// allowed domains [gmail.com, yahoo.com]

console.log(validateEmail('john.doe@yahoo.com'));
console.log(validatePassword('123'));

function validatePassword(password) {
  
  var passwordLength = password.length;
  let valid = false;

  if (passwordLength >= 8 && passwordLength <= 20) {
    valid = true;
  }

  if (password.includes('!', '@', '$')) {
    valid = true;
  }

  return  valid;
}

function validateEmail(email) {

  let valid = false;

  if (email.includes('.', '@', 'gmail.com', 'yahoo.com')) {
     valid = true;
  }

  return valid;
}
