const PASS_MIN_LENGTH = 8
const PASS_MAX_LENGTH = 20
const PASS_REQ_CHARs = ['@','!','$']
const PASS_Invalid_CHARS = ['#','%','^','&','*','(',')']

const vUtils = require('./validator-utils');
function isMoreMinThanLength(input){
    return vUtils.isMoreThanLength(input, PASS_MIN_LENGTH)
}

function isLessThanMaxLength(input){
    return vUtils.isLessThanLength(input, PASS_MAX_LENGTH)
}


function deosContainReqChar(input){
    return vUtils.doesContainsAnyofChars(input, PASS_REQ_CHARs)
}

function doesNotContainIlligealChar(input){
    return !vUtils.doesContainsAnyofChars(PASS_Invalid_CHARS)
}


module.exports={isMoreMinThanLength, isLessThanMaxLength, deosContainReqChar, doesNotContainIlligealChar}