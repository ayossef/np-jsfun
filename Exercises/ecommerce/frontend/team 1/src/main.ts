// list all prodcuts  - (1 GET)
// select and add to cart 1 iPhone and 2 S22 - (1 POST)
// make a purchase - (1 POST)

// ====================================
/**
 * IEcommerce - Interface
 *      - add to cart
 *      - list products
 *      
 *  Shop Implements IEcommerce
 *      methods:
 *        - listProducts()
 *        - addItem(Cart(itemId, quantity))
 * ===================================
 * Cart class
 *     methods:
 *          - purchase() 
 *          
 * Product/Item class
 * 
 * hold data and operate on data         
 *        
 */

import Shop from '../../../backend/Shop';
import Cart from '../../../backend/Cart';
import Product from '../../../backend/Product';


const myShop = new Shop(new Cart())

const p1 = new Product()

console.log(p1.getAllProducts())
