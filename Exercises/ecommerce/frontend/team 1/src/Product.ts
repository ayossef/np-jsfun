import Loader from './loader';

export default class Product {

    private id: number = 0;
    private name: string = '';
    private price: number = 0;
    private url: string = 'http://localhost:3000/products'

    // constructor(id?: number, name?: string, price?: number) {
    //     this.id = id;
    //     this.name = name;
    //     this.price = price;
    // }


    getProduct(): Object {

        return {}
    }

    fetchProducts(): Promise<any> {
        const response = new Loader(this.url).getJSON()

        return response.then(res => res.json()).then(data => { return data.products })
    }

    async getAllProducts(): Promise<Object[]> {
        const products = await this.fetchProducts()
        return products
    }
}