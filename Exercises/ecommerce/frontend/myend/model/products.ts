import IItem from "../utils/IItem"

export default class Product implements IItem{
    private id: number
    private name: string
    private price: number

    constructor(id:number, name:string, price:number) {
        this.name = name
        this.id = id
        this.price = price
    }

    getId():number{
        return this.id
    }
    getName(): string{
        return this.name
    }
    getPrice(): number{
        return this.price
    }

    stringfy(): string{
        return "Name: "+this.name+" is "+ this.price + " USD with Id "+this.id
    }

}