import { ItemOps } from "../utils/ItemOps";
import Product from "./products";

export default class Cart{
    private itemOpsHelper: ItemOps | undefined
    private products: Array<Product>

    constructor(){
        this.products = new Array<Product>()
        // this.itemOpsHelper = new ItemOps(this.products, 0)
    }

    addProduct(p: Product){
        this.products.push(p)
    }

    getProducts(): Array<Product>{
        return this.products
    }
    removeProduct(productId:number){
        let itemHelper = new ItemOps(this.products, productId)
        itemHelper.removeAllOccOfItem()       
    }
    decreseOccOfProduct(productId:number){
        let itemHelper = new ItemOps(this.products, productId)
        itemHelper.removeAllOccOfItem()
    }
    getQuanitiyOfProduct(productId:number){
        let itemHelper = new ItemOps(this.products, productId)
        return itemHelper.getCount()
    }

    getTotalAmount():number{
        let totalPrice: number = 0
        this.products.map((item)=> totalPrice += item.getPrice())
        return totalPrice
    }
}