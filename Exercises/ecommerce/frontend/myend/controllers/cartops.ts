import Cart from '../model/cart'
import Product from '../model/products'
import EChecker from './echcker'

export default class CartOps{
    private cart:Cart
    constructor(cart:Cart) {
        this.cart = cart
    }
    addProductToCart(p:Product): boolean{
        return new EChecker().isEligiableToAdd(p, this.cart)
    }
}