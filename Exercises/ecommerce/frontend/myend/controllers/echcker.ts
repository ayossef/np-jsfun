import Cart from "../model/cart";
import Product from "../model/products";
import { ItemOps } from "../utils/ItemOps";

export default class EChecker{
    CART_MAX_CAPCITY:number = 10
    CART_MAX_AMOUNT:number = 1000
    MAX_NUMBER_OF_OCC:number = 3
    isEligiableToAdd(p:Product, c:Cart){
        return !this.isCartFull(c) 
            && !this.doesCartContainMaxOccOfProuct(c,p)
            && !this.doesCartHaveMaxAmount(c,p)

    }
    private isCartFull(c:Cart):boolean{
        return c.getProducts().length === this.CART_MAX_CAPCITY
    }
    private doesCartHaveMaxAmount(c:Cart, p:Product){
        let totalPrice: number = 0
        c.getTotalAmount()
        return totalPrice+p.getPrice() > this.CART_MAX_AMOUNT
    }
    private doesCartContainMaxOccOfProuct(c:Cart, p:Product):boolean{
        let products = c.getProducts()
        let itemHelper = new ItemOps(products, p.getId())
        let numberOfOcc = itemHelper.getCount()
        return numberOfOcc === this.MAX_NUMBER_OF_OCC
    }
}