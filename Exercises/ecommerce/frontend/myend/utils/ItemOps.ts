import IItem from "./IItem";

export class ItemOps{

    private list:Array<IItem>
    private id:number|undefined 
    constructor(list:Array<IItem>, id:number){
        this.list = list
        this.id = id
    }
    removeAllOccOfItem(){
        for (let index = 0; index < this.list.length; index++) {
            const item = this.list[index];
            if(item.getId() === this.id){
               this.list.splice(index, 1)
               break
            }
        }
    }
    removeSingleOccOfItem(){
        for (let index = 0; index < this.list.length; index++) {
            const item = this.list[index];
            if(item.getId() === this.id){
               this.list.splice(index, 1)
               break
            }
        }
    }
    getCount():number{
        let counter:number =0
        for (let index = 0; index < this.list.length; index++) {
            const item = this.list[index];
            if(item.getId() === this.id){
               counter +=1
            }
        }
        return counter
    }
    getNumberOfOcc():number{
        let currentItemOcc = 0
        for (let index = 0; index < this.list.length; index++) {
            const item = this.list[index];
            if(item.getId() === this.id){
                currentItemOcc += 1
            }
        }
        return currentItemOcc
    }
}