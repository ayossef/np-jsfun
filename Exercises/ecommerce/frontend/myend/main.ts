import CartOps from "./controllers/cartops";
import Cart from "./model/cart";
import Product from "./model/products";
import axios, { AxiosResponse } from "axios";

console.log("Welcome to ECommerce");

let { myCartOps, tempProduct } = initValues();

myCartOps.addProductToCart(tempProduct);

var allProducts: Array<Product>;

loadAllProducts();

function initValues() {
    let tempProduct = new Product(1, "temp", 10);
    let myCart = new Cart();
    let myCartOps = new CartOps(myCart);
    return { myCartOps, tempProduct };
}

function loadAllProducts() {
    axios.get("http://localhost:3000/products").then((response: AxiosResponse) => {
        console.log(response.data);

        const products = response.data;
        allProducts = new Array<Product>();
        products.forEach((product: Product) => {
            allProducts.push(
                new Product(product["id"], product["name"], product["price"])
            );
        });

        allProducts.map((p) => console.log(p.stringfy()));
    });
}

