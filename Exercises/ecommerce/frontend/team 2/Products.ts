export default class ProductsItem {
    id:string = ""
    productName:string = ""
    price:number = 0

    constructor(id:string, productName:string ,price:number){
        this.id = id;
        this.productName = productName;
        this.price = this.price
    }
}
