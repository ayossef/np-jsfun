"use strict";
exports.__esModule = true;
var Cart = /** @class */ (function () {
    function Cart() {
    }
    Cart.prototype.getCartProduct = function (item) {
        return {};
    };
    Cart.prototype.getCartProducts = function () {
        return [];
    };
    Cart.prototype.addProduct = function (product) {
        // POST method details will go here
    };
    Cart.prototype.purchaseProducts = function () {
        // POST method to add to list of purchases
    };
    return Cart;
}());
exports["default"] = Cart;
