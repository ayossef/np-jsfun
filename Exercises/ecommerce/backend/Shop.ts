import Cart from "./Cart";
import IEcommerce from "./IEcommerce";
import axios from 'axios'
import Product from "./Product";

export default class Shop implements IEcommerce {

    cart: Cart;
    product: Product = new Product;

    constructor(cart: Cart) {
        this.cart = cart
    }

    getAllProducts(): Object[] {
        this.product.getAllProducts()
        return [{}]
    }

    addProductToCart(product: Product): void {
        this.cart.addProduct(product)
    }

    purchase() {
        this.cart.purchaseProducts()
    }

}