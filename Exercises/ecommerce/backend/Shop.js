"use strict";
exports.__esModule = true;
var Shop = /** @class */ (function () {
    function Shop(cart) {
        this.cart = cart;
    }
    Shop.prototype.getAllProducts = function () {
        return [{}];
    };
    Shop.prototype.addProductToCart = function (product) {
        this.cart.addProduct(product);
    };
    Shop.prototype.purchase = function () {
        this.cart.purchaseProducts();
    };
    return Shop;
}());
exports["default"] = Shop;
