// list of meals - fried chicken, grilled chicken => chicken, 2
mealsList = [
    { name: 'pizza', id: '001', price: 80},
    { name: 'pasta', id: '002', price: 50},
    { name: 'mogodu', id: '003', price: 100},
    { name: 'grilled chicken', id: '003', price: 150},
    { name: 'chicken', id: '004', price: 200},
    { name: 'fish', id: '005', price: 120}
];

mealToFind = 'chicken'

console.log('True find')
findAMeal(mealsList, mealToFind)
numberOfOcc(mealsList, mealToFind)
indexOfOcc(mealsList, mealToFind)


console.log('\nFalse find')
mealToFindFalse = 'zzzzzzzz'
findAMeal(mealsList, mealToFindFalse)
numberOfOcc(mealsList, mealToFindFalse)
indexOfOcc(mealsList, mealToFindFalse)

// find - prints found or not found
function findAMeal(mealsList, mealToFind){
    var flag = false;
    for (let mealIndex = 0; mealIndex < mealsList.length; mealIndex++) {

        const meal = mealsList[mealIndex];
        if (meal.name.includes(mealToFind)) {
            flag = true
            console.log('found');
            
        } 
    }
    if(flag == false){
    console.log(' not found')
    }
}

// find - number of occ
function numberOfOcc(mealsList, mealToFind){
    var counter =0;
    for (let mealIndex = 0; mealIndex < mealsList.length; mealIndex++) {

        const meal = mealsList[mealIndex];
        if (meal.name.includes(mealToFind) ) {
            counter ++;
            
            
        } 
    }
    console.log(counter)
    
}

// find - index of meal
function indexOfOcc(mealsList, mealToFind){

    listOfOcc =[];
    
    for (let mealIndex = 0; mealIndex < mealsList.length; mealIndex++) {

        const meal = mealsList[mealIndex];
    
        if (meal.name.includes(mealToFind) ) {
            listOfOcc.push(mealIndex)
            
            
        } 
    }
    console.log(listOfOcc)

}