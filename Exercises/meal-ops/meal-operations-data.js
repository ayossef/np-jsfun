const menu = [
    { name: 'fried chicken', price: 'R100' },
    { name: 'grilled chicken', price: 'R80' },
    { name: 'beef steak', price: 'R200' },
    { name: 'sticky pork chops', price: 'R185' }
]
const meal = 'chicken'

module.exports = {menu, meal}