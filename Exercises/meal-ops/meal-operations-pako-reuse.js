// find - prints found or not found
function findAMeal(mealsList, mealToFind) {
    return numberOfOcc(mealsList, mealToFind) > 0
}
// find - number of occ
function numberOfOcc(mealsList, mealToFind) {
    return findLocations(mealsList, mealToFind).length
}
// find - locations
function findLocations(mealsList, mealToFind) {
    let positions = []
    let mealIndex = 0
    for (; mealIndex < mealsList.length; mealIndex++) {
        if (mealsList[mealIndex].name.includes(mealToFind)) {
            positions.push(mealIndex)
        }
    }
    return positions
}



module.exports = {findAMeal, findLocations, numberOfOcc}