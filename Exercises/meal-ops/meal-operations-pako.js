// list of meals - fried chicken, grilled chicken => chicken, 2
menu = [
    { name: 'fried chicken', price: 'R100' },
    { name: 'grilled chicken', price: 'R80' },
    { name: 'beef steak', price: 'R200' },
    { name: 'sticky pork chops', price: 'R185' }
]

meal = 'pork'
findAMeal(menu, meal)
numberOfOcc(menu, meal)
findLocations(menu, meal)


// find - prints found or not found
function findAMeal(mealsList, mealToFind) {
    let found = false
    let mealIndex = 0
    for (; mealIndex < mealsList.length; mealIndex++) {
        if (mealsList[mealIndex].name.includes(mealToFind)) {
            found = true
            break;
        }
    }

    if (found)
        console.log('Found!')
    else
        console.log('Not found!')
}

// find - number of occ
function numberOfOcc(mealsList, mealToFind) {
    let count = 0
    for (let i = 0; i < mealsList.length; i++) {
        if (mealsList[i].name.includes(mealToFind)) {
            count++;
        }
    }
    console.log('number of occurences: '+count);
}
// find - locations
function findLocations(mealsList, mealToFind) {
    let positions = []
    let mealIndex = 0
    for (; mealIndex < mealsList.length; mealIndex++) {
        if (mealsList[mealIndex].name.includes(mealToFind)) {
            positions.push(mealIndex)
        }
    }
    console.log('postions: ['+ positions+']');
}

