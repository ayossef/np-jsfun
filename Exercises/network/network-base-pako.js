// load users from https://jsonplaceholder.typicode.com/users
// print names and emails for the full list
const URL = 'https://jsonplaceholder.typicode.com/users'

async function printUsers() {
    await fetch(URL)
        .then(res => res.json())
        .then((users) => { 
            users.forEach(user => {
              console.log(`${user.name} <---> ${user.email}`) 
            });
         })
}

// printUsers()

async function getUsers() {
    const response = await fetch(URL)
    const data = await response.json()
    console.log(data)
}
getUsers()
