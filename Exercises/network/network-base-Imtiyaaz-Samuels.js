// load users from https://jsonplaceholder.typicode.com/users
// print names and emails for the full list
getUser()

async function getUser(){
    let response = await fetch('https://jsonplaceholder.typicode.com/users')
    let data = await response.json();
    for (let index = 0; index < data.length; index++) {
        console.log("Name: "+data[index].name+" , Email: "+data[index].email);
        
    }
           
}