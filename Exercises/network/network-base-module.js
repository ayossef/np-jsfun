// Original Code
async function getUser(){
    let response = await fetch('https://jsonplaceholder.typicode.com/users')
    let data = await response.json();
    for (let index = 0; index < data.length; index++) {
        console.log("Name: "+data[index].name+" , Email: "+data[index].email);   
    }        
}

// 1. First Approach
async function fetchUser(){
    let response = await fetch('https://jsonplaceholder.typicode.com/users')
    let data = await response.json();
    return data
}
function printUser(listOfUsers){
    for (let index = 0; index < listOfUsers.length; index++) {
        console.log("Name: "+listOfUsers[index].name+" , Email: "+listOfUsers[index].email);   
    } 
}

printUser(fetchUser())


// 2. Second Approach
function fetchUrl(url){
    let response = fetch(url)
    return response
}

function fetchJsonData(url){
    return fetchUrl(url).json()
}

const USERS_URL = 'https://jsonplaceholder.typicode.com/users'
function fetchUserData(){
    return fetchJsonData(USERS_URL)
  
}

function fetchAndDisplayUserData(){
    const userDataPromise = fetchUserData()
    userDataPromise.then((data, err)=>{
        printUser(data)
    })
}