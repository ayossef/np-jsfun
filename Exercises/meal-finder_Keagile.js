// Meal should contain [{name, id, price}]


// Search function should pring all meal details if found or print 'this meal is not available'

mealsList = [
  {mealName: 'fish', id: '12', price:'R 25.00'},
  {mealName: 'pizza', id: '13', price:'R 43.56'},
  {mealName: 'pasta', id: '14', price:'R 83.12'},
  {mealName: 'mogodu pasta', id: '15', price:'R 62.34'}
];

mealToFind = 'pasta'

findAMeal(mealsList, mealToFind);
numberOfOcc(mealsList, mealToFind);

function findAMeal(mealsList, mealToFind) {
  let found = false
  let mealIndex = 0
  for ( ; mealIndex < mealsList.length; mealIndex++) {
      const meal = mealsList[mealIndex].mealName;
      if (meal === mealToFind) {
          found = true
          break;
      }
  }
  if (found)
      console.log(mealsList[mealIndex])
  else
      console.log('This meal is not available')
}

function numberOfOcc(mealsList, mealToFind){
  let occurences = 0
  for(let i = 0; i < mealsList.length; i++){
    const meal = mealsList[i].mealName;

    if(meal.includes(mealToFind)){
      occurences++
    }
  }
  console.log(mealToFind + ' appears ' + occurences + ' times.')
  
}
