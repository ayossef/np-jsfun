# Installing JS Env.

In order to bring the env. up and running we need to install:
1. node
2. editor (vscode)
3. plugins - js snippets


## Installing nodejs
In order to install nodejs, we have two options:
1. Install from Ubuntu servers
2. Install from Node servers


```bash
sudo apt update
sudo apt install nodejs
# you don't have the most updated version of nodejs 
```

```bash
curl -fsSL https://deb.nodesource.com/setup_lts.x | sudo -E bash - &&\
sudo apt-get install -y nodejs
# After installation is done, we can verify the installation
node --verison
npm --version
```


## Install VsCode 
Downloading the vscode package
```bash
sudo dpkg -i code.deb
```