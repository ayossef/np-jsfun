import axios from 'axios'

export default class Loader{
    private url:string
    constructor(url:string){
        this.url = url
    }
    async getJSON(){
        let {data, status} = await axios.get(this.url)
        return data
    }
}