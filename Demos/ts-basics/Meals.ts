export class Meal {
    price:number 
    name:string
    calories:number
    constructor(name:string, price:number, calories?:number) {
        this.name = name
        this.price = price
        calories === undefined? this.calories = 0: this.calories = calories
    }
}