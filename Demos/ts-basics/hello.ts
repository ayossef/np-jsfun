let mealName: string = 'Mogodu'
let mealPrice: number = 100

let x
x = 'hello'
x = 100

let y: string
y = 'hello'

let z: (string | number)
z = 'hello'
z = 100

// function print(param:string){}

// function print(param:number){}


function print(param: string | number) { }

function printMeal(name: string, price: number) {
    console.log("Meal: " + name + " is " + price + " USD")
}
console.log('Hello Typescript')


let meals = ['pizza', 'pasta', 'mogodu']

meals.map((meal) =>  { console.log(meal) })

