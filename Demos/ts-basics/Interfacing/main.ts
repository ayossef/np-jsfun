import Car from "./Car";
import ElecEngine from "./ElecEngine";
import ICar from "./ICar";
import NormalEngine from "./NormalEngine";
import SuperEngine from "./SuperEngine";


let carsList = [new Car('Ahmed', '123', new NormalEngine()), new Car('Pako', '34556', new SuperEngine())]
carsList.push(new Car('Keagile', '9843', new ElecEngine()))
carsList.map((car)=>{car.speedUp(); car.printDetails()})