export default interface ICar{
    speedUp()
    slowDown()
    stop()
    getDetails(): string
    getName(): string
    printDetails()
}