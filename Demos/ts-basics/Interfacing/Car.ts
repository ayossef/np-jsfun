import ICar from "./ICar";
import ISpeeder from "./IEngine";

export default class Car implements ICar{
    private speed:number =0 
    private ownerName:string
    private lpn:string
    private speeder: ISpeeder // 1 NC, 2 SC, 3 EC
    constructor(ownerName:string, lpn:string, speeder:ISpeeder){
        this.ownerName = ownerName
        this.lpn = lpn
        this.speeder = speeder
    }
    speedUp() {
       this.speed = this.speeder.speedUp(this.speed)
    }
    slowDown(){
        this.speed -= 10
    }
    stop() {
        this.speed = 0
    }
    getDetails(): string {
        let details: string =' car with LPN:'+this.lpn +' Owned By: '+this.getName()+' is going on speed of '+this.speed
        return details 
    }
    printDetails() {
       console.log(this.getDetails());
    }

    getName(): string {
        return this.ownerName
    }
    
}