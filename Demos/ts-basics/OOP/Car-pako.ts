export default class Car {

    MIN_SPEED = 0

    speed: number = 0
    lpn: string = ''
    maxSpeed: number = 0
    type: string = ''
    firstName: string
    lastName: string

    getName():string{
        return this.firstName+" "+this.lastName
    }
    constructor(ownerName:string,lpn:string,maxSpeed:number,type:string){
        this.firstName = ownerName.split(" ")[0]
        this.lastName = ownerName.split(" ")[1]
        this.lpn = lpn
        this.maxSpeed = maxSpeed
        this.type = type
    }

    // 1. Define a MAX Speed Limit
    speedUp(){
        if (this.speed < this.maxSpeed-10)
            this.speed += 10
    }

    // 2. We need to ensure that speed can't be -ve
    slowDown(){
        if (this.speed > 10)
            this.speed -= 10
    }

    stop(){
        this.speed = 0
    }
    // 3. We need to mention the type of the car - wether it is normal or super or Eco (A new class you will define)
    stringfy(){
       let details: string = this.type+' car with LPN:'+this.lpn +' Owned By: '+this.getName()+' is going on speed of '+this.speed
       return details 
    }

    printDetails(){
        console.log(this.stringfy());
    }
}