import Car from "./Car-pako";

export default class SuperCar extends Car {
    constructor(ownerName:string, lpn:string) {
        super(ownerName, lpn, 1000, 'Super')
    }
    speedUp(): void {
        this.speed += 30
    }
}