import Car from "./Car-pako";

export default class EcoCar extends Car {

    constructor(ownerName:string, lpn:string) {
        super(ownerName, lpn, 100, 'Eco')
    }
    
    speedUp(): void {
        this.speed += 5
    }
}