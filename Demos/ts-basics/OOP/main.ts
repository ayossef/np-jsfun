import Car from './Car-pako';
import SuperCar from './SuperCar-pako';
import EcoCar from './Eco-pako';

let myCar: EcoCar = new EcoCar('Ahmed Yossef', '1283932')
console.log(myCar.getName());
myCar.speedUp()
myCar.printDetails()

let yourCar: SuperCar = new SuperCar('You', '309430834')
yourCar.speedUp()
yourCar.printDetails()
