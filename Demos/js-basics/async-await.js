async function printNumbers(loopName){
    for (let index = 0; index < 1000; index++) {
        await console.log(loopName+':'+index);
    }
}

function printNumbersTwo(loopName){
    for (let index = 0; index < 1000; index++) {
        console.log(loopName+':'+index);
    }
}

printNumbers('loop one')
printNumbersTwo('loop two')
