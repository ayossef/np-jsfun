mealsList = ['pizza', 'pasta', 'mogodu', 'fish']
mealToFind = 'pasta'

console.log(mealsList);

findAMeal(mealsList, mealToFind)

function findAMeal(mealsList, mealToFind){
    for (let mealIndex = 0; mealIndex < mealsList.length; mealIndex++) {
        const meal = mealsList[mealIndex];
        if (meal === mealToFind) {
            console.log(meal);
        } else {
            console.log('Not '+mealToFind);
        }
    }
}